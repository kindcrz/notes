.container{
	display: grid;
}

Explicit Grid:
	grid-template-columns: 200px 200px 200px;
	grid-template-rows: 200px 200px 200px;

	grid-auto-rows: 100px;
		- auto sizing of the remaining/added grids(?)

	grid-column-gap: 20px;
	grid-row-gap: 20px;
	/ grid-gap: 20px


Implicit Grid:
	grid-template-auto: 200px 200px 200px;
