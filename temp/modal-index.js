import React, { Component } from 'react'
import { Modal } from 'react-bootstrap'
import { connect } from 'react-redux'
import moment from 'moment'
import ActionDAO from './actions'
import ImportTrails from './components/ImportTrails'
import InputRow from './components/InputRow'
import * as FireStarter from '../../../../../../shared/utils/FireStarter'

let Action
class ImportJob extends Component {
  constructor(props) {
    super(props)
    this.state = {
      file: '',
      import: false,
      trails: {},
      counter: 0,
      length: 0
    }
    this.getFile = ''
    this.validate = this.validate.bind(this)
  }

  componentDidMount() {
    FireStarter.getImportTrail(this.props.client, this.props.store, (snap) => {
      this.setState({
        trails: snap ? snap : {}
      })
    })
  }

  import() {

    Action = new ActionDAO(this.props.client, this.props.store)
    Action.xlsxParse(this.getFile.files[0], this.props.toast).then((file) => {
      this.setState({
        import: true,
        fileLength: file.length,
        counter: 0
      }, () => {
        this.validate(file, (rejected, counter) => {
          if (rejected.length > 0) {
            Action.exportError(rejected).then((url) => {
              this.setState({
                blob: url
              })
            })
          }

          this.setState({
            import: false,
          })

          let trail = {
            id: Object.keys(this.state.trails).length,
            fileName: this.getFile.files[0].name,
            status: rejected.length > 0 ? 'Failed' : 'Done',
            remarks: `${counter} out of ${file.length} jobs`,
            rejected,
            timeStamp: moment().valueOf()
          }

          FireStarter.pushImportTrail(this.props.client, this.props.store, trail)
        })

      })
    })
  }

  validate(file, fnCallBack) {
    let self = this
    let arr = [...file]
    let counter = 0
    let rejected = []
    const delay = () => {
      return new Promise(resolve => {
        setTimeout(resolve, 300)
      })
    }

    async function ping() {
      for (let key in arr) {

        Action.validateXLSX(arr[key], (job) => {
          FireStarter.addDelivery(self.props.client, self.props.store, job, () => {
            counter++
            self.setState({ counter })
          })
        }, (err) => {
          rejected.push(err)
        })

        await delay()
      }
      fnCallBack(rejected, counter)
    }

    ping()
  }

  exportTemplate() {
    let Action = new ActionDAO()
    Action.exportTemplate()
  }

  render() {

    return (
      <Modal
        show={this.props.showModal}
        bsSize='lg'
        dialogClassName='import-job'
      >
        <Modal.Body>
          <div
            className='font-10 closeModalButton'
            onClick={() => this.props.handleModal()}
            style={{ right: -13, top: '45%' }}
          >
            <i className='fa fa-times' />
          </div>

          <div style={{ padding: '0px 5px 10px' }}>
            Import Job
          </div>

          <hr />

          <div style={{ padding: '8px 20px' }}>
            <InputRow
              inputRef={e => this.getFile = e}
              fileHandler={() => this.import()}
              templateHandler={() => this.exportTemplate()}
            />
          </div>

          <div>
            <ImportTrails
              state={{ ...this.state }}
              getFile={this.getFile}
              trails={this.state.trails}
            />
          </div>

          <hr />
        </Modal.Body>
      </Modal>
    )
  }
}

function mapStateToProps(state) {
  return {
    client: state.userReducer.userInfo.details.client,
    store: state.userReducer.userInfo.details.stores[0]
  }
}

export default connect(mapStateToProps)(ImportJob)