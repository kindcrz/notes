Content Type
	* node
Fields
Taxonomy
Path
Add Content
Manage Display
Set Permissions
Test


Content Workflow (Taxonomy)
- Vocabulary > Terms


Views Workflow
1. Display
2. Format
3. Fields
4. Filter
5. Sort

People
1. Super User
2. Administrator (manage entire site)
3. Authenticated Users (registered & logged in users)
4. Anonymous Users (visitors; not logged in users)