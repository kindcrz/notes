npm i                         ===         yarn

npm i --save package          ===         yarn add package
npm i --save-dev package      ===         yarn add --dev package
npm i --global package        ===         yarn add global package

npm rm --save package         ===         yarn remove package

