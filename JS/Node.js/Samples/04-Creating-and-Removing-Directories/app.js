var fs = require('fs');

// SYNC
// fs.unlinkSync('readMe.txt') // Delete a file

// fs.mkdirSync('dir') // Make a directory (folder)
// fs.rmdirSync('dir') // Remove a directory (folder)

// ASYNC
// needs a callback function to run

// fs.mkdir('dir', function () {
// 	fs.readFile('readMe.txt', 'utf-8', function (err, data) {
// 		fs.writeFile('./dir/writeMe.txt', data, function (err) {
// 			console.log('New folder and file created successfully')
// 		})
// 	})
// })


// ASYNC + SYNC
// fs.rmdir('dir') // ! A directory can only be deleted if it is empty
fs.unlink('./dir/writeMe.txt', function(){ // Async
	fs.rmdirSync('dir') // Sync
})