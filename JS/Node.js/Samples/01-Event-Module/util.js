// var events = require('events')
// var util = require('util') // used to inherit the EventEmitter to Person
var events = require('events')
var util = require('util') // used to inherit the EventEmitter to Person

var Person = function (name) {
	this.name = name
}

// util.inherits(Person, events.EventEmitter)
util.inherits(Person, events.EventEmitter)

var kwon = new Person('kwonfiyaaah')
var lee = new Person('lee_uji')
var choi = new Person('choi_scheol95')

var people = [kwon, lee, choi]

people.forEach(function (person) {
	person.on('speak', function (msg) {
		console.log(person.name + ': ' + msg)
	})
})

kwon.emit('speak', 'no need to rush')
setTimeout(function () {
	lee.emit('speak', 'cause you can')
}, 1500)
setTimeout(function () {
	choi.emit('speak', 'wait what?')
}, 4000)
// lee.emit('speak', 'cause you can')
// choi.emit('speak', 'wait what?')