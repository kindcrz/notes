var events = require('events')

var myEmmiter = new events.EventEmitter()
myEmmiter.on('someEvent', function(msg){
	console.log(msg)
})

myEmmiter.emit('someEvent', 'The event was emitted')