var counter = function (svt) {
	return 'There are ' + svt.length + ' leaders in Seventeen';
}

var adder = function (a, b) {
	return `The sum of ${a} and ${b} is ${a+b}` //ES6: back-ticks; "template string"
}

var pi = 3.142;

// module.exports = counter;

module.exports.counter = counter;
module.exports.adder = adder;
module.exports.pi = pi;
