var fs = require('fs')//File System


// Sync = "Synchronous"
// processes run one after the other
// "blocking code"

// var readMe = fs.readFileSync('readMe.txt', 'utf-8') //Read a File
// 	//fs.readFileSync('filename', 'encoding')

// // console.log(readMe);

// fs.writeFileSync('writeMe.txt', readMe)
// 	//fs.writeFileSync('filename', content)

// Async
// processes run at the same time (in the backgro und)
// "non-blocking code"

fs.readFile('readMe.txt', 'utf-8', function(err, data){
	fs.writeFile('writeMe.txt', data)
})
